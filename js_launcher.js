// js_launcher.js

const { spawn } = require('child_process');
const tasks = [
	{
		"task_name": "Vecteur_1_1",
		"run": () => spawn('./VecteurGPU', ['image1.pgm', '1', '1'])
	},
	{
		"task_name": "Vecteur_1_23",
		"run": () => spawn('./VecteurGPU', ['image1.pgm', '1', '23'])
	},
	{
		"task_name": "Vecteur_23_1",
		"run": () => spawn('./VecteurGPU', ['image1.pgm', '23', '1'])
	},
	{
		"task_name": "Vecteur_23_23",
		"run": () => spawn('./VecteurGPU', ['image1.pgm', '23', '23'])
	},
	{
		"task_name": "Stavro_1_1",
		"run": () => spawn('./VecteurGPU', ['stavrovouni.pgm', '1', '1'])
	},
	{
		"task_name": "Stavro_1_23",
		"run": () => spawn('./VecteurGPU', ['stavrovouni.pgm', '1', '23'])
	},
	{
		"task_name": "Stavro_23_1",
		"run": () => spawn('./VecteurGPU', ['stavrovouni.pgm', '23', '1'])
	},
	{
		"task_name": "Stavro_23_23",
		"run": () => spawn('./VecteurGPU', ['stavrovouni.pgm', '23', '23'])
	},
	{
		"task_name": "Montagne_1_1",
		"run": () => spawn('./VecteurGPU', ['MontagneFoncee.pgm', '1', '1'])
	},
	{
		"task_name": "Montagne_1_23",
		"run": () => spawn('./VecteurGPU', ['MontagneFoncee.pgm', '1', '23'])
	},
	{
		"task_name": "Montagne_23_1",
		"run": () => spawn('./VecteurGPU', ['MontagneFoncee.pgm', '23', '1'])
	},
	{
		"task_name": "Montagne_23_23",
		"run": () => spawn('./VecteurGPU', ['MontagneFoncee.pgm', '23', '23'])
	},
	/*,
	{
		"task_name": "Stavro",
		"run": () => spawn('./LanceTaches', ['stavrovouni.pgm'])
	},
	{
		"task_name": "Montagne",
		"run": () => spawn('./LanceTaches', ['MontagneFoncee.pgm'])
	}
	*/
]

/**
 * 
 * @param {Number} nbIterations : Number of times a single task
 * must be runned as a child process
 */
const run_tasks_and_do_processing_time_mean = (nbIterations)=>{
	let async_mean_treatments = tasks.map((t)=>{
		return new Promise((finalResolve, reject)=>{
			let async_processes = [];

			for (let i = 0; i < nbIterations; i++) {
				async_processes.push(new Promise((res,rej)=>{
					
					const task = t.run();
					task.on('error', (err)=>{
						console.error(`${t.task_name}, ERROR on iteration n°${i}: `, err);
						// process.exit(1);
						res(0);
					});
				
					task.on('close', (code) => {
						console.log(`task ${t.task_name} it.${i} returned measure: ${code}`);
						res(code);
					});
				}));
			}

			Promise.all(async_processes).then((resultArray)=>{
				const processing_time_mean = {};
				let sum = 0;

				resultArray.forEach((value)=>{
					sum += value;
				});
				processing_time_mean[t.task_name] = sum / nbIterations;
				finalResolve(processing_time_mean);
			});
		});
	});

	Promise.all(async_mean_treatments)
	.then((result)=>{
		console.log("result===",result);
	});
}
spawn('make', ['clean']).on('close', (code)=> {
	if (code !== 0) console.log(`make clean exited with code ${code}`);
	else {
		spawn('make').on('close', (code2)=>{
			if (code2 !== 0) console.log(`Make exited with code ${code}`);
			else run_tasks_and_do_processing_time_mean(5);
		});
	}
});


