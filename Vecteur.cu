#include <stdio.h>
#include <sys/time.h>

#include <stdlib.h>
#include <time.h>
#include <sys/resource.h>
#include <unistd.h>

#define initTimer struct timeval tv1, tv2; struct timezone tz
#define startTimer gettimeofday(&tv1, &tz)
#define stopTimer gettimeofday(&tv2, &tz)
#define tpsCalcul (tv2.tv_sec-tv1.tv_sec)*1000000L + (tv2.tv_usec-tv1.tv_usec)


#define MAX_DIM_GRID 65535
#define MAX_DIM_BLOCK 1024



#define MAX_CHAINE 100

#define MAX_HOSTS 100



#define CALLOC(ptr, nr, type) 		if (!(ptr = (type *) calloc((size_t)(nr), sizeof(type)))) {		\
						printf("Erreur lors de l'allocation memoire \n") ; 		\
						exit (-1);							\
					} 


#define FOPEN(fich,fichier,sens) 	if ((fich=fopen(fichier,sens)) == NULL) { 				\
						printf("Probleme d'ouverture du fichier %s\n",fichier);		\
						exit(-1);							\
					} 
				
#define MIN(a, b) 	(a < b ? a : b)
#define MAX(a, b) 	(a > b ? a : b)

#define MAX_VALEUR 	255
#define MIN_VALEUR 	0

#define NBPOINTSPARLIGNES 18

#define false 0
#define true 1
#define boolean int




long tailleimg_vect ;

/* KERNEL CUDA */

__global__ void add_vec_scalaire_gpu(int *vec, int *res, int a, long N) {
	long i = (long)blockIdx.x * (long)blockDim.x + (long)threadIdx.x;
	if (i < N) {
		res[i] = vec[i] + a;
	}
}

__global__ void rehaussement_contraste_gpu(int *vec, int *res, int Min, float etalement, long vecSize) {
	long i = (long)blockIdx.x * (long)blockDim.x + (long)threadIdx.x;
	if (i < vecSize) {
		res[i] = (vec[i] - Min) * (int)etalement;
	}
}


void add_vec_scalaire_cpu(int *vec, int *res, int a, long N) 
{
	int i ;
	for (i=0 ; i < N ; i ++) {
		res[i] = vec[i] + a;
	}
}

int main(int argc, char *argv[]) {
	int minimum = 0, maximum = 255;
	
	
	/*========================================================================*/
	/* Declaration de variables et allocation memoire */
	/*========================================================================*/

	int i, n;
	
	int LE_MIN = MAX_VALEUR;
	int LE_MAX = MIN_VALEUR;
	
	float ETALEMENT = 0.0;
	
	int *image;
	int *resultat;
	int X, Y, x, y;
	int TailleImage;
	
	int P;
	
	FILE *Src, *Dst;

	char SrcFile[MAX_CHAINE];
	char DstFile[MAX_CHAINE];
	
	char ligne[MAX_CHAINE];
	
	boolean inverse = false;
	
	char *Chemin;
	char *CheminTache;
	
	
	
	
	
	int alpha = 10;
	if (argc < 2) {
		// printf("Erreur, manque un argument\n");
		exit(0);
	}
	tailleimg_vect = atol(argv[1]);
	long blocksize = 1;	
	if (argc ==3) {
		blocksize = atoi(argv[2]);
	}

	int *img_vect;
	int *resultat_img_vect;
	int *cudaVec;
	int *cudaRes;










	/*========================================================================*/
	/* Recuperation des parametres						*/
	/*========================================================================*/

	sscanf(argv[1],"%s", SrcFile);
	
	sprintf(DstFile,"%s.new",SrcFile);
	
	/*========================================================================*/
	/* Recuperation de l'endroit ou l'on travail				*/
	/*========================================================================*/

	CALLOC(Chemin, MAX_CHAINE, char);
	CALLOC(CheminTache, MAX_CHAINE, char);
	Chemin = getenv("PWD");
	// printf("Repertoire de travail : %s \n\n",Chemin);
	

	/*========================================================================*/
	/* Ouverture des fichiers						*/
	/*========================================================================*/

	// printf("Operations sur les fichiers\n");

	FOPEN(Src, SrcFile, "r");
	// printf("\t Fichier source ouvert (%s) \n",SrcFile);
		
	FOPEN(Dst, DstFile, "w");
	// printf("\t Fichier destination ouvert (%s) \n",DstFile);
	
	/*========================================================================*/
	/* On effectue la lecture du fichier source */
	/*========================================================================*/
	
	// printf("\t Lecture entete du fichier source ");
	
	for (i = 0 ; i < 2 ; i++) {
		fgets(ligne, MAX_CHAINE, Src);	
		fprintf(Dst,"%s", ligne);
	}	

	fscanf(Src," %d %d\n",&X, &Y);
	fprintf(Dst," %d %d\n", X, Y);
	
	fgets(ligne, MAX_CHAINE, Src);	/* Lecture du 255 	*/
	fprintf(Dst,"%s", ligne);
	
	// printf(": OK \n");
	
	/*========================================================================*/
	/* Allocation memoire pour l'image source et l'image resultat 		*/
	/*========================================================================*/
	
	TailleImage = X * Y;
	CALLOC(image, TailleImage+1, int);
	memset(image, 0, TailleImage);
	
	CALLOC(resultat, TailleImage+1, int);
	memset(resultat, 0, TailleImage);
	
	// printf("\t\t Initialisation de l'image [%d ; %d] : Ok \n", X, Y);
			
	
	x = 0;
	
	/*========================================================================*/
	/* Lecture du fichier pour remplir l'image source 			*/
	/*========================================================================*/
	
	while (! feof(Src)) {
		n = fscanf(Src,"%d",&P);
		image[x] = P;	
		LE_MIN = MIN(LE_MIN, P);
		LE_MAX = MAX(LE_MAX, P);
		x ++;
		if (n == EOF || (x == TailleImage-1)) {
			break;
		}
	}
	fclose(Src);
	// printf("\t Lecture du fichier image : Ok \n\n");
	
	/*========================================================================*/
	/* Calcul du facteur d'etalement					*/
	/*========================================================================*/
	
	if (inverse) {
		ETALEMENT = 0.2;	
	} else {
		ETALEMENT = (float)(MAX_VALEUR - MIN_VALEUR) / (float)(LE_MAX - LE_MIN);
		// facteur d'étalement = (255-0) / (55-35)
		//(pixel - 35) * facteur_etalement
	}
	
	
	








	initTimer;

	long size = sizeof(int)*TailleImage;

	//img_vect = (int *) malloc(size);
	resultat_img_vect = (int *)malloc(size);

/*
	if (img_vect == NULL) {
		// printf("Allocation memoire qui pose probleme (img_vect) \n");
	}
	*/
	if (resultat_img_vect == NULL) {
		// printf("Allocation memoire qui pose probleme (resultat_img_vect) \n");
	}
	int default_val_img = rand() % 100, default_val_result = 0;
	//memset(img_vect, default_val_img, TailleImage);
	memset(resultat_img_vect, default_val_result, TailleImage);

/*	cudaSetDevice(1);	*/

	if (cudaMalloc((void **)&cudaVec, size) == cudaErrorMemoryAllocation) {
		// printf("Allocation memoire qui pose probleme (cudaVec) \n");
	}
	if (cudaMalloc((void **)&cudaRes, size)  == cudaErrorMemoryAllocation) {
		// printf("Allocation memoire qui pose probleme (cudaRes) \n");
	}

	long dimBlock = blocksize;
	long dimGrid = TailleImage/blocksize;
	if ((TailleImage % blocksize) != 0) {
		dimGrid++;
	}



	int res = cudaMemcpy(&cudaVec[0], &image[0], size, cudaMemcpyHostToDevice);

	// printf("Copy CPU -> GPU %d \n",res);
/*
startTimer;
	add_vec_scalaire_gpu<<<dimGrid, dimBlock>>>(cudaVec, cudaRes, alpha, tailleimg_vect);
stopTimer;
*/
startTimer;
	rehaussement_contraste_gpu<<<dimGrid, dimBlock>>>(cudaVec, cudaRes, minimum, ETALEMENT, tailleimg_vect);
stopTimer;
	// int *vec, int *res, int Min, int etalement, long vecSize

	cudaMemcpy(&resultat_img_vect[0], &cudaRes[0], size, cudaMemcpyDeviceToHost);


	/* Test bon fonctionnement */

	bool ok = true;
	int indice = 0, goodResult = 0, g;
	/*
	for (i= 0 ; i < TailleImage; i++) {
/*		printf("Resultat GPU %d     Resultat CPU %d \n",resultat[i], img_vect[i]+alpha);	
		goodResult = (img_vect[i] - minimum) * ETALEMENT;
		if (resultat_img_vect[i] != goodResult) {
			ok = false;
			indice += 1;
		}
		goodResult = 0;
	}
	*/
	// printf("------ ");
	// printf("dimGrid %ld dimBlock %ld ",dimGrid, dimBlock);
	if (ok) {
		// printf("Resultat ok\n");
		for (g=0; g<TailleImage; g++){
			// printf("%d ", resultat_img_vect[g]);
			if (g == X) {
				// printf("\n");
			}
		}
	} else {
		// printf("resultat NON ok (%d erreurs)\n", indice);
	}
	// printf("img_vect %ld => Temps calcul GPU %ld \n", tailleimg_vect, tpsCalcul);



	cudaFree(cudaVec);
	cudaFree(cudaRes);



/*========================================================================*/
	/* Sauvegarde de l'image dans le fichier resultat			*/
	/*========================================================================*/
	
	
	n = 0;
	for (i = 0 ; i < TailleImage ; i++) {
		fprintf(Dst,"%3d ",resultat_img_vect[i]);
		n++;
		if (n == NBPOINTSPARLIGNES) {
			n = 0;
			fprintf(Dst, "\n");
		}
	}
	
				
	fprintf(Dst,"\n");
	fclose(Dst);
	
	// printf("\n");

	/*========================================================================*/
	/* Fin du programme principal	*/
	/*========================================================================*/
	return tpsCalcul;
	exit(0);
}




