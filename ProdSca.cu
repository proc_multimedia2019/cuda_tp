#include <stdio.h>
#include <sys/time.h>


#define initTimer struct timeval tv1, tv2; struct timezone tz
#define startTimer gettimeofday(&tv1, &tz)
#define stopTimer gettimeofday(&tv2, &tz)
#define tpsCalcul (tv2.tv_sec-tv1.tv_sec)*1000000L + (tv2.tv_usec-tv1.tv_usec)





/******************************************************************/
/*** KERNEL CUDA															*/


__global__ void mult_vecteur(int *vec1, int *vec2, int *res, int N) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < N) {
		res[i] = vec1[i] * vec2[i];
	}
	__syncthreads();
}



__global__ void SommeDeuxElementsVecteur(int *vec, int N) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < N) {
		vec[i] = vec[i] + vec[i+N];
	}
	__syncthreads();
}


/******************************************************************/

/******************************************************************/
/*** Programme principal													*/


int main(int argc, char *argv[]) {

	initTimer;

	int tailleVecteur ;
	int tailleCalcul ;

/*	if (argc < 2) {
		printf("Erreur, manque un argument\n");
		exit(0);
	}
*/
	/* Recuperation des parametres de lancement de la commande */
	
/*	tailleVecteur = atoi(argv[1]);			*/

	/* On fige la taille du probleme a 32 elements dans le vecteur */
	tailleVecteur = 32;	

	tailleCalcul = tailleVecteur;

	int blocksize = 4;
	
	if (argc ==3) {
		blocksize = atoi(argv[2]);
	}

	/* Pointeurs vers les vecteurs cote CPU */
	
	int *vecteur1, *vecteur2;
	int *resultat;

	/* Pointeurs vers les vecteurs cote GPU */
	
	int *cudaVec1, *cudaVec2;
	int *cudaRes;

	/* Variables utiles pour mesurer le temps de calul */




	/* Allocation memoire cote CPU	*/
	int size = sizeof(int)*tailleVecteur;
	vecteur1 = (int *)malloc(size);
	vecteur2 = (int *)malloc(size);
	resultat = (int *)malloc(size);

	/* Allocation memoire cote GPU 	*/
	cudaMalloc((void **)&cudaVec1, size);
	cudaMalloc((void **)&cudaVec2, size);
	cudaMalloc((void **)&cudaRes, size);

	/* Initialisation des deux vecteurs */
	/* Les deux vecteurs ont des valeurs comprises entre [-10 ; 10]	*/
	int i ;
	for (i= 0 ; i < tailleVecteur ; i++) {
		vecteur1[i] = rand() % 20 - 10;
		vecteur2[i] = rand() % 20 - 10;
		resultat[i] = 0;
	}

	/* Selection de la carte graphique 	*/
	cudaSetDevice(0);

	/* Calcul de la dimension des blocks	*/
	/* Les block sont de  dimension 4, ou egale a l'argument passe en parametre	*/
	int dimBlock = blocksize;
	int dimGrid = tailleVecteur/blocksize;

	/* Si la taille du vecteur n'est pas un multiple de la taille des blocks, 
	il faut un block de plus */
	
	if ((tailleVecteur % blocksize) != 0) {
		dimGrid++;
	}

	/* Copie des vecteurs cote GPU	*/
	
	cudaMemcpy(cudaVec1, &vecteur1[0], size, cudaMemcpyHostToDevice);
	cudaMemcpy(cudaVec2, &vecteur2[0], size, cudaMemcpyHostToDevice);

	/* Lancement du kernel sur une grille 1D	*/
	
startTimer;
	mult_vecteur<<<dimGrid, dimBlock>>>(cudaVec1, cudaVec2, cudaRes, tailleVecteur);



stopTimer;
	/* Copie du resultat de la carte graphique vers le processeur 	*/
	/* Cette partie n'est pas utile pour le calcul	
		mais permet de verifier que le calcul de multiplication
		est correct		*/

	cudaMemcpy(resultat, &cudaRes[0], size, cudaMemcpyDeviceToHost);

	/* Partie du code qui verifie le resultat de la multiplication 	*/
	
	bool ok = true;
	
	for (i= 0 ; i < tailleVecteur ; i++) {
		if (resultat[i] != vecteur1[i] * vecteur2[i]) {
			ok = false;
		}
	}
	printf("------ ");
	printf("dimGrid %d dimBlock %d ",dimGrid, dimBlock);
	if (ok) {
		printf("Resultat ok\n");
	} else {
		printf("resultat NON ok\n");
	}

	printf("Vecteur %ld => Temps total  CPU %ld \n", tailleVecteur, tpsCalcul);





	/* Calcul des sommes des elements du vecteur resultat (somme 2 a 2) */
	/* Le calcul est effectue en enchainant les calculs 
		sur une taille de vecteur de plus en plus petite*/
	/* Ici, le vecteur etant de taille 16, 
		on a 4 etapes de calcul a faire */

	


	SommeDeuxElementsVecteur<<<dimGrid, dimBlock>>>(cudaRes, 16);
	SommeDeuxElementsVecteur<<<dimGrid, dimBlock>>>(cudaRes, 8);
	SommeDeuxElementsVecteur<<<dimGrid, dimBlock>>>(cudaRes, 4);
	SommeDeuxElementsVecteur<<<dimGrid, dimBlock>>>(cudaRes, 2);
	SommeDeuxElementsVecteur<<<dimGrid, dimBlock>>>(cudaRes, 1);

	/* On recupere le resultat final, qui n'est qu'un simple entier	*/
	/* Donc copie d'un seul element du vecteur resultat */



	cudaMemcpy(resultat, &cudaRes[0], sizeof(int), cudaMemcpyDeviceToHost);

	/* Liberation de la memoire cote GPU	*/
	cudaFree(cudaVec1);
	cudaFree(cudaVec2);
	cudaFree(cudaRes);

	/* Verification du calcul des sommes et du produit scalaire	*/

	int prod = 0;
	for (i= 0 ; i < tailleVecteur ; i++) {
		prod = prod + vecteur1[i] * vecteur2[i];
	}

	printf("Produit scalaire CPU %d\n",prod);
	printf("Produit scalaire GPU %d\n",resultat[0]);


}



